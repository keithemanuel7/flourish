﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Flourish.Services
{
    /// <summary>
    /// Class for storing files using the file system.
    /// </summary>
    public class FileStorage : IStorage
    {
        public string RootPath { get; private set; }

        public FileStorage(string rootPath)
        {
            RootPath = rootPath;
        }

        public void Save(string relativePath, IStorable storable)
        {
            string filePath = Path.Combine(RootPath, relativePath, storable.FileName);
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            File.WriteAllBytes(filePath, storable.Data);
        }

        public void SaveRange(string relativePath, ICollection<IStorable> storables)
        {
            foreach(IStorable storable in storables)
            {
                Save(relativePath, storable);
            }
        }
        
        public IStorable Load(string relativePath, string fileName)
        {
            string filePath = Path.Combine(RootPath, relativePath, fileName);

            IStorable storable = new Storable(
                fileName, 
                File.ReadAllBytes(filePath), 
                File.GetCreationTimeUtc(filePath));

            return storable;
        }

        public void Delete(string relativePath, string fileName)
        {
            File.Delete(Path.Combine(RootPath, relativePath, fileName));
        }

        public void Delete(string relativePath, IStorable storable)
        {
            Delete(relativePath, storable.FileName);
        }

        public ICollection<IStorable> LoadPath(string path)
        {
            ICollection<IStorable> storables = new List<IStorable>();

            foreach (string filePath in Directory.EnumerateFiles(Path.Combine(RootPath, path)))
            {
                IStorable storable = new Storable(
                    Path.GetFileName(filePath),
                    File.ReadAllBytes(filePath),
                    File.GetCreationTimeUtc(filePath));

                storables.Add(storable);
            }

            return storables;
        }
        
        public ICollection<IStorable> LoadPaths(List<string> paths)
        {
            var storables = new List<IStorable>();

            foreach(string path in paths)
            {
                storables.AddRange(LoadPath(path));
            }

            return storables;
        }
    }
}