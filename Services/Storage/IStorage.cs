﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flourish.Database.Models;

namespace Flourish.Services
{
    /// <summary>
    /// Describes a way to store files. For example, a file system or database.
    /// </summary>
    public interface IStorage
    {
        void Save(string relativePath, IStorable storable);

        void SaveRange(string relativePath, ICollection<IStorable> storables);
        
        IStorable Load(string relativePath, string fileName);

        void Delete(string relativePath, string fileName);

        void Delete(string relativePath, IStorable storable);

        ICollection<IStorable> LoadPath(string path);

        ICollection<IStorable> LoadPaths(List<string> paths);
    }
}
