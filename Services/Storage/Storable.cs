﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Flourish.Services
{
    /// <summary>
    /// Simple implementation of IStorable.
    /// </summary>
    public class Storable : IStorable
    {
        public string FileName { get; set; }

        public byte[] Data { get; set; }

        public DateTime FileDate { get; set; }

        public string Extension {
            get
            {
                return Path.GetExtension(FileName);
            }
        }

        public string FileNameWithoutExtension
        {
            get
            {
                return Path.GetFileNameWithoutExtension(FileName);
            }
        }

        public Storable(string fileName, byte[] data, DateTime fileDate)
        {
            FileName = fileName;
            Data = data;
            FileDate = fileDate;
        }

        public Storable(string fileName, string data, DateTime fileDate)
            : this(fileName, Encoding.UTF8.GetBytes(data), fileDate)
        {
        }
    }
}
