﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flourish.Services
{
    /// <summary>
    /// Describes a file.
    /// </summary>
    public interface IStorable
    {
        string FileName { get; set; }

        string FileNameWithoutExtension { get; }

        string Extension { get; }

        byte[] Data { get; set; }

        DateTime FileDate { get; set; }
    }
}
