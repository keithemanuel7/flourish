﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Services.Extensions
{
    /// <summary>
    /// Extension methods for DateTimes.
    /// </summary>
    public static class DateTimeExtentions
    {
        /// <summary>
        /// Converts a DateTime to a string using a standardized format.
        /// </summary>
        /// <param name="dateTime">This DateTime object.</param>
        /// <returns>A standardized string.</returns>
        public static string ToStandardizedString(this DateTime dateTime)
        { 
            return dateTime.ToString("yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture);
        }
    }
}
