﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    /// <summary>
    /// Methods for extracting data from a Collection of ServiceParameterModels.
    /// </summary>
    public static class ServiceParameterModelCollectionExtensions
    {
        public static dynamic GetValue(this ICollection<ServiceParameterModel> spm, ParameterModel pm)
        {
            return spm.GetValue(pm.Category, pm.Key);
        }

        public static dynamic GetValue(this ICollection<ServiceParameterModel> spm, string key)
        {
            return spm.GetValue(null, key);
        }

        public static dynamic GetValue(this ICollection<ServiceParameterModel> spm, string category, string key)
        {
            ServiceParameterModel model = spm.Single(
                x => x.Category == category && x.Key == key);

            return Convert.ChangeType(model.Value, Type.GetType(model.Type));
        }

        public static Dictionary<string, dynamic> GetCategory(this ICollection<ServiceParameterModel> spm, ParameterModel pm)
        {
            return spm.GetCategory(pm.Category);
        }

        public static Dictionary<string, dynamic> GetCategory(this ICollection<ServiceParameterModel> spm, string category)
        {
            return spm
                .Where(p => p.Category == category)
                .ToDictionary(k => k.Key, v => Convert.ChangeType(v.Value, Type.GetType(v.Type)));
        }

        public static bool Exists(this ICollection<ServiceParameterModel> spm, string key)
        {
            return spm.Exists(null, key);
        }

        public static bool Exists(this ICollection<ServiceParameterModel> spm, string category, string key)
        {
            return spm.Any(p => p.Category == category && p.Key == key);
        }

        public static bool CategoryExists(this ICollection<ServiceParameterModel> spm, string category)
        {
            return spm.Any(p => p.Category == category);
        }
    }
}
