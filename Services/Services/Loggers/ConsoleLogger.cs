﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    public class ConsoleLogger : IServiceLogger
    {
        public ConsoleLogger()
        {
        }

        public void Log(LogLevel level, string message, Exception exception = null)
        {
            Console.WriteLine($"{level}: {message}");

            if(exception != null)
            {
                Console.WriteLine($"EXCEPTION: {message}\n{exception.StackTrace}");
            }
        }

        public void Debug(string message)
        {
            Log(LogLevel.Debug, message);
        }

        public void Info(string message)
        {
            Log(LogLevel.Info, message);
        }

        public void Warn(string message)
        {
            Log(LogLevel.Warn, message);
        }

        public void Error(string message, Exception exception = null)
        {
            Log(LogLevel.Error, message, exception);
        }

        public void Fatal(string message, Exception exception = null)
        {
            Log(LogLevel.Fatal, message, exception);
        }

        public void Event(ServiceLogEvent serviceLogEvent, string message = null, Exception exception = null)
        {
            Console.WriteLine($"EVENT - {serviceLogEvent}: {message}");
        }
    }
}
