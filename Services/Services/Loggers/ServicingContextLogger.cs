﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    public class ServicingContextLogger : IServiceLogger
    {
        private ServicingContext _context;
        private int _serviceId;
        private int? _serviceJobId;

        public ServicingContextLogger(ServicingContext context, ServiceHandler serviceHandler)
        {
            _context = context;
            _serviceId = serviceHandler.ServiceModel.ServiceId;
            _serviceJobId = serviceHandler.ServiceJobModel?.ServiceJobId;
        }

        private void Log(LogLevel level, string message, ServiceLogEvent? serviceLogEvent, Exception exception)
        {
            ExceptionLogModel insertedExceptionLog = null;

            if (exception != null)
            {
                insertedExceptionLog = _context.ExceptionLogs.InsertException(exception).Entity;
            }

            _context.ServiceLogs.Add(new ServiceLogModel()
            {
                ServiceId = _serviceId,
                ServiceJobId = _serviceJobId,
                EventType = serviceLogEvent?.ToString(),
                LogLevel = LogLevel.Info.ToString(),
                Message = message,
                ExceptionLog = insertedExceptionLog,
                Timestamp = DateTimeOffset.Now,
            });

            _context.SaveChangesAsync();
        }

        public void Log(LogLevel level, string message, Exception exception = null)
        {
            Log(level, message, null, exception);
        }

        public void Event(ServiceLogEvent serviceLogEvent, string message = null, Exception exception = null)
        {
            Log(LogLevel.Info, message, serviceLogEvent, exception);
        }

        public void Debug(string message)
        {
            Log(LogLevel.Debug, message);
        }

        public void Info(string message)
        {
            Log(LogLevel.Info, message);
        }

        public void Warn(string message)
        {
            Log(LogLevel.Warn, message);
        }

        public void Error(string message, Exception exception = null)
        {
            Log(LogLevel.Error, message, exception);
        }

        public void Fatal(string message, Exception exception = null)
        {
            Log(LogLevel.Fatal, message, exception);
        }

    }
}
