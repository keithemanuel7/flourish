﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flourish.Services
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
    }

    public enum ServiceLogEvent
    {
        ServiceCreated,
        JobCreated,
        ScheduleStarted,
        ScheduleStopped,
        ScheduleTriggered,
        ExecutionStarted,
        ExecutionSuccessful,
        ExecutionFailed,
        FileExtracted,
        NoFileToExtract,
    }

    /// <summary>
    /// Describes an interface that a Service and its Jobs can use for logging.
    /// </summary>
    public interface IServiceLogger
    {
        void Log(LogLevel level, string message, Exception exception = null);
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message, Exception exception);
        void Fatal(string message, Exception exception);
        void Event(ServiceLogEvent serviceLogEvent, string message = null, Exception exception = null);
    }
}
