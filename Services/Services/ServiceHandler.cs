﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    /// <summary>
    /// Helps create Services.
    /// </summary>
    public class ServiceHandler
    {
        public ServiceModel ServiceModel { get; set; }
        public ServiceJobModel ServiceJobModel { get; set; }
        public IStorage Storage { get; set; }
        public IServiceLogger Logger { get; set; }

        public ServiceHandler(ServiceModel serviceModel, IStorage storage, IServiceLogger logger)
        {
            ServiceModel = serviceModel;
            Storage = storage;
            Logger = logger;
        }

        public ServiceHandler(ServiceModel serviceModel, IStorage storage, IServiceLogger logger, ServiceJobModel serviceJobModel)
            : this(serviceModel, storage, logger)
        {
            ServiceJobModel = serviceJobModel;
        }

        public ServiceHandler BuildForJob(ServiceJobModel serviceJobModel)
        {
            return new ServiceHandler(ServiceModel, Storage, Logger, serviceJobModel);
        }
    }
}
