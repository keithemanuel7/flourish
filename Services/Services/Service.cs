﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flourish.Database.Models;
using System.Threading;

namespace Flourish.Services
{
    /// <summary>
    /// A Service controls its Jobs and executes on a Schedule.
    /// 
    /// When created, the service will create instances of all of the jobs that belong to it, as well as start processings and data that has not been fully processed.
    /// When the schedule elapses, the root Jobs (Jobs with no parent) will be triggered. 
    /// </summary>
    public class Service : IHasSchedule
    {
        public ServiceHandler ServiceHandler { get; private set; }
        public ServiceModel ServiceModel { get; private set; }
        public ISchedule Schedule { get; private set; }
        public IStorage Storage { get; private set; }
        public IServiceLogger Logger { get; protected set; }

        protected ICollection<ServiceParameterModel> Parameters { get; private set; }

        public Service(ServiceHandler serviceHandler)
        {
            ServiceHandler = serviceHandler;
            ServiceModel = serviceHandler.ServiceModel;
            Storage = serviceHandler.Storage;
            Logger = serviceHandler.Logger;
            
            Parameters = ServiceModel.Parameters;

            Type scheduleType = Type.GetType(ServiceModel.Schedule.ClassName);
            Schedule = (ISchedule)Activator.CreateInstance(
                scheduleType,
                new ScheduleHandler(
                    Execute, 
                    ServiceModel.Parameters.Where(p => p.Parameter.ScheduleId != null).ToList()));
            
            ConfigureJobs();
            TriggerChildJobs();
        }

        /// <summary>
        /// Creates and sets the instances for all of this Service's Jobs.
        /// </summary>
        protected void ConfigureJobs()
        {
            foreach (ServiceJobModel serviceJob in ServiceModel.ServiceJobs)
            {
                Type jobType = Type.GetType(serviceJob.Job.ClassName);
                serviceJob.JobInstance = (JobBase)Activator.CreateInstance(jobType, ServiceHandler.BuildForJob(serviceJob));
            }
        }

        /// <summary>
        /// Executes this Service. Called by the Schedule when it is invoked.
        /// </summary>
        protected void Execute()
        {
            TriggerRootJobs();
        }

        /// <summary>
        /// Triggers an Execution of all of the root Jobs. The Jobs are responsible for triggering their childrens executions, which may also be automatic.
        /// </summary>
        protected void TriggerRootJobs()
        {
            foreach (ServiceJobModel serviceJob in ServiceModel.ServiceJobs.Where(sj => sj.ParentServiceJobMappings.Count == 0))
            {
                serviceJob.JobInstance.Trigger();
            }
        }

        /// <summary>
        /// Triggers an execution of the child (non-root) Jobs. This will finish processing any extracted, but not fully processed, files. 
        /// </summary>
        protected void TriggerChildJobs()
        {
            foreach (ServiceJobModel serviceJob in ServiceModel.ServiceJobs.Where(sj => sj.ParentServiceJobMappings.Count > 0))
            {
                serviceJob.JobInstance.Trigger();
            }
        }
    }
}
