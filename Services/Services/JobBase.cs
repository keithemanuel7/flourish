﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Flourish.Database.Models;
using Flourish.Services.Extensions;

namespace Flourish.Services
{
    /// <summary>
    /// Base class for Jobs.
    /// </summary>
    public abstract class JobBase : IJob
    {
        public string ClassName { get; private set; }

        public ServiceModel ServiceModel { get; private set; }
        public ServiceJobModel ServiceJobModel { get; private set; }
        public IServiceLogger Logger { get; private set; }
        public IStorage Storage { get; private set; }

        public string InputPath { get; private set; }
        public ICollection<string> OutputPaths { get; private set; }

        public ICollection<ServiceParameterModel> ServiceParameters { get; private set; }
        public ICollection<ServiceParameterModel> JobParameters { get; private set; }

        public JobBase(ServiceHandler serviceBuilder)
        {
            ClassName = GetType().Name;

            ServiceModel = serviceBuilder.ServiceModel;
            ServiceJobModel = serviceBuilder.ServiceJobModel;
            Storage = serviceBuilder.Storage;
            Logger = serviceBuilder.Logger;

            ServiceParameters = ServiceModel.Parameters.Where(p => p.Parameter.JobId == null && p.Parameter.ScheduleId == null).ToList();
            JobParameters = ServiceModel.Parameters.Where(p => p.Parameter.JobId == ServiceJobModel.JobId).ToList();

            // TODO: These paths should be unique, in case the same job is used twice in a single service.
            InputPath = Path.Combine(ServiceModel.Name, ServiceJobModel.Job.Name, "Input");

            OutputPaths = new List<string>();
            foreach (ServiceJobModel childServiceJob in ServiceJobModel.ChildServiceJobs)
            {
                OutputPaths.Add(Path.Combine(ServiceModel.Name, childServiceJob.Job.Name, "Input"));
            }

            VerifyRequirements();

            Logger.Event(ServiceLogEvent.JobCreated);
        }

        protected abstract void ExecuteJob();

        public void Trigger()
        {
            Execute();
        }

        public void Execute()
        {
            Logger.Event(ServiceLogEvent.ExecutionStarted);

            try
            {
                ExecuteJob();
                Logger.Event(ServiceLogEvent.ExecutionSuccessful);

                // Trigger an execution of the child jobs.
                foreach (ServiceJobModel childServiceJob in ServiceJobModel.ChildServiceJobs)
                {
                    childServiceJob.JobInstance.Trigger();
                }

                OnSuccess();
            }
            catch (Exception ex)
            {
                Logger.Event(ServiceLogEvent.ExecutionFailed, exception: ex);
                OnFailure(ex);
            }
        }

        public virtual void OnSuccess() { }

        public virtual void OnFailure(Exception ex) { }

        protected virtual void VerifyRequirements()
        {
            var unmetRequirements = new List<string>();

            foreach (ParameterModel jobParameter in ServiceJobModel.Job.Parameters.Where(p => p.Required))
            {
                if (!JobParameters.Exists(jobParameter.Category, jobParameter.Key))
                {
                    unmetRequirements.Add($"Required parameter '{jobParameter.Key}' does not exists.");
                }
            }
            
            if (unmetRequirements.Count > 0)
            {
                throw new UnmetRequirementsException(unmetRequirements);
            }
        }

        public void Store(ICollection<IStorable> extractedFiles)
        {
            if (ServiceJobModel.ArchiveOutput)
            {
                string storagePath = Path.Combine(ServiceModel.Name, ServiceJobModel.Job.Name, "Archive");

                foreach (IStorable storable in extractedFiles)
                {
                    Storage.Save(
                        Path.Combine(storagePath, storable.FileDate.Year.ToString(), storable.FileDate.Month.ToString()),
                        storable);
                }
            }

            foreach (string path in OutputPaths)
            {
                Storage.SaveRange(path, extractedFiles);
            }
        }

        public IStorable CreateStorable(byte[] fileData, string fileExtension)
        {
            DateTime now = DateTime.Now;

            if (!fileExtension.StartsWith("."))
            {
                fileExtension = "." + fileExtension;
            }

            return new Storable(now.ToStandardizedString() + fileExtension, fileData, now);
        }

        public IStorable CreateStorable(string fileData, string fileExtension)
        {
            return CreateStorable(Encoding.UTF8.GetBytes(fileData), fileExtension);
        }
    }
}
