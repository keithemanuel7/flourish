﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;
using System.IO;

namespace Flourish.Services
{
    /// <summary>
    /// Base class for loader Jobs.
    /// </summary>
    public abstract class LoaderBase : JobBase
    {
        public LoaderBase(ServiceHandler serviceBuilder)
            : base(serviceBuilder)
        {
        }

        public abstract void Load(IStorable file);

        /// <summary>
        /// TODO: Error Handling
        /// </summary>
        protected sealed override void ExecuteJob()
        {
            ICollection<IStorable> filesToLoad = Storage.LoadPath(InputPath);

            foreach (IStorable file in filesToLoad)
            {
                Load(file);
                Storage.Delete(InputPath, file);
            }
        }
    }
}
