﻿using Flourish.Database.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Flourish.Services.Extensions;

namespace Flourish.Services
{
    /// <summary>
    /// Base class for transformer Jobs.
    /// </summary>
    public abstract class TransformerBase : JobBase
    {
        public TransformerBase(ServiceHandler serviceBuilder)
            : base(serviceBuilder)
        { }

        public abstract IStorable Transform(IStorable storable);

        /// <summary>
        /// TODO: Error Handling
        /// </summary>
        protected sealed override void ExecuteJob()
        {
            ICollection<IStorable> inputFiles = Storage.LoadPath(InputPath);
            ICollection<IStorable> outputFiles = new List<IStorable>();

            foreach (IStorable file in inputFiles)
            {
                outputFiles.Add(Transform(file));
            }

            Store(outputFiles);
        }
    }
}
