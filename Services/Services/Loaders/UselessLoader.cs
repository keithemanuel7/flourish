﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    /// <summary>
    /// Doesn't actually do anything. Used for debugging.
    /// </summary>
    public class UselessLoader : LoaderBase
    {
        public UselessLoader(ServiceHandler serviceBuilder)
            : base(serviceBuilder) { }

        public override void Load(IStorable file)
        {
            
        }
    }
}
