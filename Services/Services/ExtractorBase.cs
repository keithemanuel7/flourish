﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flourish.Database.Models;
using Flourish.Services.Extensions;
using System.IO;

namespace Flourish.Services
{
    /// <summary>
    /// Base class for extractor Jobs.
    /// </summary>
    public abstract class ExtractorBase : JobBase
    {
        public ExtractorBase(ServiceHandler serviceBuilder)
            : base(serviceBuilder)
        { 
        }

        protected abstract ICollection<IStorable> Extract();

        /// <summary>
        /// TODO: Add retry for storage
        /// </summary>
        protected sealed override void ExecuteJob()
        {
            ICollection<IStorable> extractedFiles = Extract();

            if(extractedFiles == null || extractedFiles.Count == 0)
            {
                Logger.Event(ServiceLogEvent.NoFileToExtract);
            }
            else
            {
                string s = extractedFiles.Count == 1 ? "" : "s";
                string fileNames = string.Join("; ", extractedFiles.Select(f => f.FileName));
                string message = $"{extractedFiles.Count} file{s} extracted. Filename{s}: {fileNames}";

                Logger.Event(ServiceLogEvent.FileExtracted, message: message);

                try
                {
                    Store(extractedFiles);
                }
                catch(Exception ex)
                {
                    Logger.Fatal("Extractor encountered fatal exception when storing files.", ex);
                }
            }
        }
    }
}
