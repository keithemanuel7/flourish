﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Flourish.Database.Models;
using Flourish.Services.Extensions;

namespace Flourish.Services
{
    /// <summary>
    /// Generic Http downloader class. Supports multiple URLs, and CredentialSets (TODO).
    /// </summary>
    public class HttpDownloader : ExtractorBase
    {
        private Dictionary<string, string> _urls;
        private string _fileExtension;

        public HttpDownloader(ServiceHandler serviceBuilder)
            : base(serviceBuilder)
        {
            _fileExtension = JobParameters.GetValue("FileExtension");
            _urls = new Dictionary<string, string>();

            foreach (KeyValuePair<string, dynamic> kvp in JobParameters.GetCategory("Url"))
            {
                _urls.Add(kvp.Key, kvp.Value);
            }
        }    

        // todo: add credential support
        protected override ICollection<IStorable> Extract()
        {
            List<IStorable> downloadedFiles = new List<IStorable>();

            using (HttpClient client = new HttpClient())
            {
                foreach(KeyValuePair<string, string> kvp in _urls)
                {
                    byte[] data = client.GetByteArrayAsync(kvp.Value).Result;

                    DateTime now = DateTime.Now;
                    string fileName = now.ToStandardizedString();

                    if (!string.IsNullOrWhiteSpace(kvp.Key))
                    {
                        fileName += "_" + kvp.Key;
                    }

                    fileName += _fileExtension;

                    downloadedFiles.Add(new Storable(fileName, data, now));
                }
            }

            return downloadedFiles;
        }
    }
}
