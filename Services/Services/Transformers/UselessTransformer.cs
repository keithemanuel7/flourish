﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    /// <summary>
    /// Doesn't actually do anything. Used for debugging.
    /// </summary>
    public class UselessTransformer : TransformerBase
    {
        public UselessTransformer(ServiceHandler serviceBuilder)
            : base(serviceBuilder) { }

        public override IStorable Transform(IStorable storable)
        {
            return storable;
        }
    }
}