﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flourish.Services
{
    //[Serializable]
    public class UnmetRequirementsException : Exception
    {
        public string ResourceReferenceProperty { get; set; }

        public UnmetRequirementsException(string message)
            : base(message)
        {
        }

        public UnmetRequirementsException(ICollection<string> messages)
            : base(string.Join(Environment.NewLine, messages))
        {
        }

        public UnmetRequirementsException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public UnmetRequirementsException(ICollection<string> messages, Exception inner)
            : base(string.Join(Environment.NewLine, messages), inner)
        {
        }

        /*
        protected UnmetRequirementsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            ResourceReferenceProperty = info.GetString("ResourceReferenceProperty");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("ResourceReferenceProperty", ResourceReferenceProperty);
            base.GetObjectData(info, context);
        }
        */
    }
}
