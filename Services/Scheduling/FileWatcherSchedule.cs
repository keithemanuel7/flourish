﻿using Flourish.Database.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Flourish.Services
{
    /// <summary>
    /// A schedule that triggers when a change happens in the folders watched by the FileSystemWatchers.
    /// </summary>
    public class FileWatcherSchedule : ISchedule
    {
        public IList<FileSystemWatcher> FileSystemWatchers;

        public TimeSpan PollingTimeSpan { get; private set; }

        public ICollection<ServiceParameterModel> Parameters { get; private set; }

        private Timer _pollingTimer;

        private Action Action;

        public bool IsRunning { get; private set; }

        public FileWatcherSchedule(ScheduleHandler handler)
        {
            Action = handler.Action;
            PollingTimeSpan = handler.PollingTimeSpan ?? TimeSpan.FromMinutes(5);

            foreach(string watchPath in Parameters.GetCategory(WatchPathCategory).Values)
            {
                FileSystemWatcher watcher = new FileSystemWatcher(watchPath)
                {
                    NotifyFilter = NotifyFilters.LastWrite,
                };

                watcher.Changed += new FileSystemEventHandler(OnChanged); 
                FileSystemWatchers.Add(watcher);
            }

            IsRunning = false;
        }

        public void Start()
        {
            if (!IsRunning)
            {
                IsRunning = true;

                foreach (FileSystemWatcher fsw in FileSystemWatchers)
                {
                    fsw.EnableRaisingEvents = true;
                }

                _pollingTimer = new Timer(TimerElasped, null, PollingTimeSpan, PollingTimeSpan);
            }
        }

        public void Stop()
        {
            if (IsRunning)
            {
                IsRunning = false;

                foreach (FileSystemWatcher fsw in FileSystemWatchers)
                {
                    fsw.EnableRaisingEvents = false;
                }

                _pollingTimer.Dispose();
            }
        }

        public void Trigger()
        {
            if (IsRunning)
            {
                Action.Invoke();
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            Trigger();
        }

        private void TimerElasped(object obj)
        {
            Trigger();
        }

        protected static ParameterModel WatchPathCategory = new ParameterModel()
        {
            Category = "WatchPath",
            Description = "Category for paths to watch for the FileSystemWatcher Schedule",
            Type = "System.String",
            Required = true,
        };

        protected static ParameterModel PollingMinutesParameter = new ParameterModel()
        {
            Key = "PollingTimespanSeconds",
            Description = "The number of seconds between polling the watch paths. Default is 5 minutes.",
            Type = "System.Int32",
            Required = false,
        };

        public static IList<ParameterModel> GetParameters()
        {
            return new List<ParameterModel>()
            {
                WatchPathCategory,
                PollingMinutesParameter,
            };
        }
    }
}
