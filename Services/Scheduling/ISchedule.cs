﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Text;

namespace Flourish.Services
{
    /// <summary>
    /// Describes a Schedule to be used by Services.
    /// </summary>
    public interface ISchedule
    {
        /// <summary>
        /// Returns true if the schedule is started or false if stopped.
        /// </summary>
        bool IsRunning { get; }
        
        /// <summary>
        /// Starts the schedule.
        /// </summary>
        void Start();
        
        /// <summary>
        /// Stops the schedule.
        /// </summary>
        void Stop();

        /// <summary>
        /// Triggers an execution of the scheduled item. Does nothing if the schedule is currently executing.
        /// </summary>
        void Trigger();
    }
}
