﻿using Flourish.Database.Models;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Text;

namespace Flourish.Services
{
    /// <summary>
    /// Handles the creation of Schedule classes.
    /// </summary>
    public class ScheduleHandler
    {
        public string Id { get; set; }
        public Action Action { get; set; }
        public ICollection<ServiceParameterModel> Parameters { get; set; }
        public IServiceLogger Logger { get; set; }
        public IJobCancellationToken JobCancellationToken { get; set; }
        public TimeSpan? PollingTimeSpan { get; set; }

        public ScheduleHandler(Action action, ICollection<ServiceParameterModel> parameters = null)
        {
            Action = action;
            Parameters = parameters ?? new List<ServiceParameterModel>();
        }
    }
}
