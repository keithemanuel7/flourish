﻿using System;
using System.Collections.Generic;
using System.Text;
using Flourish.Database.Models;

namespace Flourish.Services
{
    /// <summary>
    /// A Schedule that can only be Triggered.
    /// </summary>
    public class TriggeredSchedule : ISchedule
    {
        public Action Action { get; private set; }
        public JobModel JobModel { get; private set; }
        public IServiceLogger Logger { get; set; }

        public bool IsRunning { get; private set; }

        public TriggeredSchedule(JobModel jobModel, IServiceLogger logger = null)
        {
            JobModel = jobModel;
            IsRunning = false;

            Logger = logger ?? new ConsoleLogger();
        }

        public void Start()
        {
            throw new NotImplementedException("Triggered Schedule does not implement Start()");
        }

        public void Stop()
        {
            throw new NotImplementedException("Triggered Schedule does not implement Stop()");
        }

        public void Trigger()
        {
            Logger.Event(ServiceLogEvent.ScheduleStarted);
            Action.Invoke();
        }
    }
}
