﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flourish.Services
{
    /// <summary>
    /// Represents that a class has a Schedule.
    /// </summary>
    public class IHasSchedule
    {
        ISchedule Schedule { get; }
    }
}
