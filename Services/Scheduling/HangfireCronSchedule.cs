﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Flourish.Database.Models;
using Hangfire;

namespace Flourish.Services
{
    /// <summary>
    /// A cron Schedule using Hangfire.
    /// </summary>
    public class HangfireCronSchedule : ISchedule
    {
        private string _actionId;

        public string CronExpression { get; private set; }
        public Action Action { get; set; }
        public IJobCancellationToken CancellationToken { get; private set; }
        public IServiceLogger Logger { get; set; }
        public ICollection<ServiceParameterModel> Parameters { get; private set; }

        public bool IsRunning { get; private set; }

        public HangfireCronSchedule(ScheduleHandler handler) 
        {
            _actionId = Parameters.GetValue(ActionIdParameter) ?? handler.Id;
            Parameters = handler.Parameters;
            Action = handler.Action;
            CronExpression = Parameters.GetValue(CronExpressionParameter);
            CancellationToken = handler.JobCancellationToken ?? JobCancellationToken.Null;
            Logger = handler.Logger ?? new ConsoleLogger();
            
            IsRunning = false;
        }

        private void Execute()
        {
            Action.Invoke();
        }

        public void Start()
        {
            if (!IsRunning)
            {
                IsRunning = true;
                CancellationToken = new JobCancellationToken(false);
                RecurringJob.AddOrUpdate(
                    _actionId,
                    () => Execute(),
                    CronExpression);
                Logger.Event(ServiceLogEvent.ScheduleStarted);
            }
        }

        public void Stop()
        {
            if (IsRunning)
            {
                IsRunning = false;
                CancellationToken = new JobCancellationToken(true);
                RecurringJob.RemoveIfExists(_actionId);
                Logger.Event(ServiceLogEvent.ScheduleStopped);
            }
        }

        public void Trigger()
        {
            RecurringJob.Trigger(_actionId);
            Logger.Event(ServiceLogEvent.ScheduleTriggered);
        }

        protected static ParameterModel CronExpressionParameter = new ParameterModel()
        {
            Key = "CronExpression",
            Description = "Cron expression used by the Hangfire schedule.",
            Type = "System.String",
            Required = true,
        };

        protected static ParameterModel ActionIdParameter = new ParameterModel()
        {
            Key = "ActionId",
            Description = "Unique ActionId used to identify the job in Hangfire. This field is not required because a default will be used unless another value is supplied.",
            Type = "System.String",
            Required = false,
        };

        public static IList<ParameterModel> GetParameters()
        {
            return new List<ParameterModel>()
            {
                CronExpressionParameter,
                ActionIdParameter,
            };
        }
    }
}
