﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Flourish.Database.Migrations
{
    public partial class ServicingMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Servicing");

            migrationBuilder.CreateTable(
                name: "Certificate",
                schema: "Servicing",
                columns: table => new
                {
                    CertificateId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    File = table.Column<byte[]>(nullable: true),
                    FileExtension = table.Column<string>(nullable: true),
                    Thumbprint = table.Column<string>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certificate", x => x.CertificateId);
                });

            migrationBuilder.CreateTable(
                name: "ExceptionLog",
                schema: "Servicing",
                columns: table => new
                {
                    ExceptionLogId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    InnerExceptionExceptionLogId = table.Column<int>(nullable: true),
                    InnerExceptionLogId = table.Column<int>(nullable: true),
                    Message = table.Column<string>(nullable: false),
                    StackTrace = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExceptionLog", x => x.ExceptionLogId);
                    table.ForeignKey(
                        name: "FK_ExceptionLog_ExceptionLog_InnerExceptionExceptionLogId",
                        column: x => x.InnerExceptionExceptionLogId,
                        principalSchema: "Servicing",
                        principalTable: "ExceptionLog",
                        principalColumn: "ExceptionLogId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobTypes",
                columns: table => new
                {
                    JobTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTypes", x => x.JobTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Schedule",
                schema: "Servicing",
                columns: table => new
                {
                    ScheduleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassName = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedule", x => x.ScheduleId);
                });

            migrationBuilder.CreateTable(
                name: "Service",
                schema: "Servicing",
                columns: table => new
                {
                    ServiceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Desciption = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service", x => x.ServiceId);
                });

            migrationBuilder.CreateTable(
                name: "CredentialSet",
                schema: "Servicing",
                columns: table => new
                {
                    CredentialSetId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CertificateId = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CredentialSet", x => x.CredentialSetId);
                    table.ForeignKey(
                        name: "FK_CredentialSet_Certificate_CertificateId",
                        column: x => x.CertificateId,
                        principalSchema: "Servicing",
                        principalTable: "Certificate",
                        principalColumn: "CertificateId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Job",
                schema: "Servicing",
                columns: table => new
                {
                    JobId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassName = table.Column<string>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    JobTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Job", x => x.JobId);
                    table.ForeignKey(
                        name: "FK_Job_JobTypes_JobTypeId",
                        column: x => x.JobTypeId,
                        principalTable: "JobTypes",
                        principalColumn: "JobTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Parameter",
                schema: "Servicing",
                columns: table => new
                {
                    ParameterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    JobId = table.Column<int>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Required = table.Column<bool>(nullable: false),
                    ScheduleId = table.Column<int>(nullable: true),
                    Type = table.Column<string>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameter", x => x.ParameterId);
                    table.ForeignKey(
                        name: "FK_Parameter_Job_JobId",
                        column: x => x.JobId,
                        principalSchema: "Servicing",
                        principalTable: "Job",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Parameter_Schedule_ScheduleId",
                        column: x => x.ScheduleId,
                        principalSchema: "Servicing",
                        principalTable: "Schedule",
                        principalColumn: "ScheduleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceJob",
                schema: "Servicing",
                columns: table => new
                {
                    ServiceJobId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    ArchiveOutput = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    RetryAttempts = table.Column<int>(nullable: false),
                    RetryTimespan = table.Column<TimeSpan>(nullable: false),
                    ScheduleId = table.Column<int>(nullable: false),
                    ServiceId = table.Column<int>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceJob", x => x.ServiceJobId);
                    table.ForeignKey(
                        name: "FK_ServiceJob_Job_JobId",
                        column: x => x.JobId,
                        principalSchema: "Servicing",
                        principalTable: "Job",
                        principalColumn: "JobId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceJob_Schedule_ScheduleId",
                        column: x => x.ScheduleId,
                        principalSchema: "Servicing",
                        principalTable: "Schedule",
                        principalColumn: "ScheduleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceJob_Service_ServiceId",
                        column: x => x.ServiceId,
                        principalSchema: "Servicing",
                        principalTable: "Service",
                        principalColumn: "ServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceParameter",
                schema: "Servicing",
                columns: table => new
                {
                    ServiceParameterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    Key = table.Column<string>(nullable: false),
                    ParameterId = table.Column<int>(nullable: true),
                    ServiceId = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceParameter", x => x.ServiceParameterId);
                    table.ForeignKey(
                        name: "FK_ServiceParameter_Parameter_ParameterId",
                        column: x => x.ParameterId,
                        principalSchema: "Servicing",
                        principalTable: "Parameter",
                        principalColumn: "ParameterId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceParameter_Service_ServiceId",
                        column: x => x.ServiceId,
                        principalSchema: "Servicing",
                        principalTable: "Service",
                        principalColumn: "ServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobCredential",
                schema: "Servicing",
                columns: table => new
                {
                    JobCredentialId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CredentialSetId = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    ServiceJobId = table.Column<int>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobCredential", x => x.JobCredentialId);
                    table.ForeignKey(
                        name: "FK_JobCredential_CredentialSet_CredentialSetId",
                        column: x => x.CredentialSetId,
                        principalSchema: "Servicing",
                        principalTable: "CredentialSet",
                        principalColumn: "CredentialSetId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobCredential_ServiceJob_ServiceJobId",
                        column: x => x.ServiceJobId,
                        principalSchema: "Servicing",
                        principalTable: "ServiceJob",
                        principalColumn: "ServiceJobId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceJobMapping",
                schema: "Servicing",
                columns: table => new
                {
                    ServiceJobMappingId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChildServiceJobId = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    ParentServiceJobId = table.Column<int>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceJobMapping", x => x.ServiceJobMappingId);
                    table.ForeignKey(
                        name: "FK_ServiceJobMapping_ServiceJob_ChildServiceJobId",
                        column: x => x.ChildServiceJobId,
                        principalSchema: "Servicing",
                        principalTable: "ServiceJob",
                        principalColumn: "ServiceJobId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceJobMapping_ServiceJob_ParentServiceJobId",
                        column: x => x.ParentServiceJobId,
                        principalSchema: "Servicing",
                        principalTable: "ServiceJob",
                        principalColumn: "ServiceJobId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceLog",
                schema: "Servicing",
                columns: table => new
                {
                    ServiceLogId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTimeOffset>(nullable: false),
                    DateModified = table.Column<DateTimeOffset>(nullable: false),
                    EventType = table.Column<string>(nullable: true),
                    ExceptionLogId = table.Column<int>(nullable: true),
                    LogLevel = table.Column<string>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    ServiceId = table.Column<int>(nullable: false),
                    ServiceJobId = table.Column<int>(nullable: true),
                    Timestamp = table.Column<DateTimeOffset>(nullable: false),
                    UserCreated = table.Column<string>(nullable: true),
                    UserModified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceLog", x => x.ServiceLogId);
                    table.ForeignKey(
                        name: "FK_ServiceLog_ExceptionLog_ExceptionLogId",
                        column: x => x.ExceptionLogId,
                        principalSchema: "Servicing",
                        principalTable: "ExceptionLog",
                        principalColumn: "ExceptionLogId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceLog_ServiceJob_ServiceJobId",
                        column: x => x.ServiceJobId,
                        principalSchema: "Servicing",
                        principalTable: "ServiceJob",
                        principalColumn: "ServiceJobId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CredentialSet_CertificateId",
                schema: "Servicing",
                table: "CredentialSet",
                column: "CertificateId");

            migrationBuilder.CreateIndex(
                name: "IX_ExceptionLog_InnerExceptionExceptionLogId",
                schema: "Servicing",
                table: "ExceptionLog",
                column: "InnerExceptionExceptionLogId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCredential_CredentialSetId",
                schema: "Servicing",
                table: "JobCredential",
                column: "CredentialSetId");

            migrationBuilder.CreateIndex(
                name: "IX_JobCredential_ServiceJobId",
                schema: "Servicing",
                table: "JobCredential",
                column: "ServiceJobId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_JobTypeId",
                schema: "Servicing",
                table: "Job",
                column: "JobTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Parameter_JobId",
                schema: "Servicing",
                table: "Parameter",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_Parameter_ScheduleId",
                schema: "Servicing",
                table: "Parameter",
                column: "ScheduleId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceJobMapping_ChildServiceJobId",
                schema: "Servicing",
                table: "ServiceJobMapping",
                column: "ChildServiceJobId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceJobMapping_ParentServiceJobId",
                schema: "Servicing",
                table: "ServiceJobMapping",
                column: "ParentServiceJobId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceJob_JobId",
                schema: "Servicing",
                table: "ServiceJob",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceJob_ScheduleId",
                schema: "Servicing",
                table: "ServiceJob",
                column: "ScheduleId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceJob_ServiceId",
                schema: "Servicing",
                table: "ServiceJob",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceLog_ExceptionLogId",
                schema: "Servicing",
                table: "ServiceLog",
                column: "ExceptionLogId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceLog_ServiceJobId",
                schema: "Servicing",
                table: "ServiceLog",
                column: "ServiceJobId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceParameter_ParameterId",
                schema: "Servicing",
                table: "ServiceParameter",
                column: "ParameterId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceParameter_ServiceId",
                schema: "Servicing",
                table: "ServiceParameter",
                column: "ServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobCredential",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "ServiceJobMapping",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "ServiceLog",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "ServiceParameter",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "CredentialSet",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "ExceptionLog",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "ServiceJob",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "Parameter",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "Certificate",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "Service",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "Job",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "Schedule",
                schema: "Servicing");

            migrationBuilder.DropTable(
                name: "JobTypes");
        }
    }
}
