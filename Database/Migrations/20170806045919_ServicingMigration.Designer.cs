﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Flourish.Database.Models;

namespace Flourish.Database.Migrations
{
    [DbContext(typeof(ServicingContext))]
    [Migration("20170806045919_ServicingMigration")]
    partial class ServicingMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Flourish.Database.Models.CertificateModel", b =>
                {
                    b.Property<int>("CertificateId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<byte[]>("File");

                    b.Property<string>("FileExtension");

                    b.Property<string>("Thumbprint");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("CertificateId");

                    b.ToTable("Certificate","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.CredentialSetModel", b =>
                {
                    b.Property<int>("CredentialSetId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CertificateId");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Label");

                    b.Property<string>("Password");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.Property<string>("Username");

                    b.HasKey("CredentialSetId");

                    b.HasIndex("CertificateId");

                    b.ToTable("CredentialSet","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ExceptionLogModel", b =>
                {
                    b.Property<int>("ExceptionLogId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<int?>("InnerExceptionExceptionLogId");

                    b.Property<int?>("InnerExceptionLogId");

                    b.Property<string>("Message")
                        .IsRequired();

                    b.Property<string>("StackTrace")
                        .IsRequired();

                    b.Property<string>("Type")
                        .IsRequired();

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ExceptionLogId");

                    b.HasIndex("InnerExceptionExceptionLogId");

                    b.ToTable("ExceptionLog","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.JobCredentialModel", b =>
                {
                    b.Property<int>("JobCredentialId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CredentialSetId");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<int>("ServiceJobId");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("JobCredentialId");

                    b.HasIndex("CredentialSetId");

                    b.HasIndex("ServiceJobId");

                    b.ToTable("JobCredential","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.JobModel", b =>
                {
                    b.Property<int>("JobId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClassName")
                        .IsRequired();

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<int>("JobTypeId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("JobId");

                    b.HasIndex("JobTypeId");

                    b.ToTable("Job","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.JobTypeModel", b =>
                {
                    b.Property<int>("JobTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Name");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("JobTypeId");

                    b.ToTable("JobTypes");
                });

            modelBuilder.Entity("Flourish.Database.Models.ParameterModel", b =>
                {
                    b.Property<int>("ParameterId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Description");

                    b.Property<int?>("JobId");

                    b.Property<string>("Key");

                    b.Property<bool>("Required");

                    b.Property<int?>("ScheduleId");

                    b.Property<string>("Type")
                        .IsRequired();

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ParameterId");

                    b.HasIndex("JobId");

                    b.HasIndex("ScheduleId");

                    b.ToTable("Parameter","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ScheduleModel", b =>
                {
                    b.Property<int>("ScheduleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClassName")
                        .IsRequired();

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Description")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ScheduleId");

                    b.ToTable("Schedule","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceJobMappingModel", b =>
                {
                    b.Property<int>("ServiceJobMappingId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ChildServiceJobId");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<int>("ParentServiceJobId");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ServiceJobMappingId");

                    b.HasIndex("ChildServiceJobId");

                    b.HasIndex("ParentServiceJobId");

                    b.ToTable("ServiceJobMapping","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceJobModel", b =>
                {
                    b.Property<int>("ServiceJobId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<bool>("ArchiveOutput");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<int>("JobId");

                    b.Property<int>("RetryAttempts");

                    b.Property<TimeSpan>("RetryTimespan");

                    b.Property<int>("ScheduleId");

                    b.Property<int>("ServiceId");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ServiceJobId");

                    b.HasIndex("JobId");

                    b.HasIndex("ScheduleId");

                    b.HasIndex("ServiceId");

                    b.ToTable("ServiceJob","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceLogModel", b =>
                {
                    b.Property<int>("ServiceLogId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("EventType");

                    b.Property<int?>("ExceptionLogId");

                    b.Property<string>("LogLevel")
                        .IsRequired();

                    b.Property<string>("Message");

                    b.Property<int>("ServiceId");

                    b.Property<int?>("ServiceJobId");

                    b.Property<DateTimeOffset>("Timestamp");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ServiceLogId");

                    b.HasIndex("ExceptionLogId");

                    b.HasIndex("ServiceJobId");

                    b.ToTable("ServiceLog","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceModel", b =>
                {
                    b.Property<int>("ServiceId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Desciption")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Priority");

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.HasKey("ServiceId");

                    b.ToTable("Service","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceParameterModel", b =>
                {
                    b.Property<int>("ServiceParameterId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category");

                    b.Property<DateTimeOffset>("DateCreated");

                    b.Property<DateTimeOffset>("DateModified");

                    b.Property<string>("Key")
                        .IsRequired();

                    b.Property<int?>("ParameterId");

                    b.Property<int>("ServiceId");

                    b.Property<string>("Type")
                        .IsRequired();

                    b.Property<string>("UserCreated");

                    b.Property<string>("UserModified");

                    b.Property<string>("Value")
                        .IsRequired();

                    b.HasKey("ServiceParameterId");

                    b.HasIndex("ParameterId");

                    b.HasIndex("ServiceId");

                    b.ToTable("ServiceParameter","Servicing");
                });

            modelBuilder.Entity("Flourish.Database.Models.CredentialSetModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.CertificateModel", "Certificate")
                        .WithMany()
                        .HasForeignKey("CertificateId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Flourish.Database.Models.ExceptionLogModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.ExceptionLogModel", "InnerException")
                        .WithMany()
                        .HasForeignKey("InnerExceptionExceptionLogId");
                });

            modelBuilder.Entity("Flourish.Database.Models.JobCredentialModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.CredentialSetModel", "CredentialSet")
                        .WithMany()
                        .HasForeignKey("CredentialSetId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Flourish.Database.Models.ServiceJobModel", "ServiceJob")
                        .WithMany()
                        .HasForeignKey("ServiceJobId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Flourish.Database.Models.JobModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.JobTypeModel", "JobType")
                        .WithMany()
                        .HasForeignKey("JobTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Flourish.Database.Models.ParameterModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.JobModel", "Job")
                        .WithMany("Parameters")
                        .HasForeignKey("JobId");

                    b.HasOne("Flourish.Database.Models.ScheduleModel", "Schedule")
                        .WithMany()
                        .HasForeignKey("ScheduleId");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceJobMappingModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.ServiceJobModel", "ChildServiceJob")
                        .WithMany("ChildServiceJobMappings")
                        .HasForeignKey("ChildServiceJobId");

                    b.HasOne("Flourish.Database.Models.ServiceJobModel", "ParentServiceJob")
                        .WithMany("ParentServiceJobMappings")
                        .HasForeignKey("ParentServiceJobId");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceJobModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.JobModel", "Job")
                        .WithMany()
                        .HasForeignKey("JobId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Flourish.Database.Models.ScheduleModel", "Schedule")
                        .WithMany()
                        .HasForeignKey("ScheduleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Flourish.Database.Models.ServiceModel", "Service")
                        .WithMany("ServiceJobs")
                        .HasForeignKey("ServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceLogModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.ExceptionLogModel", "ExceptionLog")
                        .WithMany()
                        .HasForeignKey("ExceptionLogId");

                    b.HasOne("Flourish.Database.Models.ServiceJobModel", "ServiceJob")
                        .WithMany()
                        .HasForeignKey("ServiceJobId");
                });

            modelBuilder.Entity("Flourish.Database.Models.ServiceParameterModel", b =>
                {
                    b.HasOne("Flourish.Database.Models.ParameterModel", "Parameter")
                        .WithMany()
                        .HasForeignKey("ParameterId");

                    b.HasOne("Flourish.Database.Models.ServiceModel", "Service")
                        .WithMany("Parameters")
                        .HasForeignKey("ServiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
