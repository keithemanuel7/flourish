﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Flourish.Database.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ServicingContext(serviceProvider.GetRequiredService<DbContextOptions<ServicingContext>>()))
            using (var transaction = context.Database.BeginTransaction())
            {
                // Look for any services.
                if (context.Services.Any())
                {
                    return;   // DB has been seeded
                }

                // Add some schedules
                ScheduleModel cronSchedule = new ScheduleModel()
                {
                    Name = "Cron Schedule",
                    ClassName = "Flourish.Services.HangfireCronSchedule",
                    Description = "Cron expression based Hangfire schedule.",
                };

                var cronExpressionParameter = new ParameterModel()
                {
                    Schedule = cronSchedule,
                    Description = "Cron expression to be passed to Hangfire for scheduling.",
                    Key = "Cron Expression",
                    Required = true,
                    Type = "System.String",
                };

                ScheduleModel fileWatcherSchedule = new ScheduleModel()
                {
                    Name = "File Watcher Schedule",
                    ClassName = "Flourish.Services.FileWatcherSchedule",
                    Description = "File watcher schedule.",
                };

                context.Schedules.AddRange(cronSchedule, fileWatcherSchedule);

                // Add some job types
                JobTypeModel extractorJobType = new JobTypeModel() { Name = "Task", };
                JobTypeModel transformerJobType = new JobTypeModel() { Name = "Extractor", };
                JobTypeModel loaderJobType = new JobTypeModel() { Name = "Transformer", };
                JobTypeModel taskJobType = new JobTypeModel() { Name = "Loader", };

                context.JobTypes.AddRange(extractorJobType, transformerJobType, loaderJobType, taskJobType);

                // Add some jobs and parameters
                var urlParameter = new ParameterModel()
                {
                    Category = "Url",
                    Description = $"The urls which requests will be sent to. 'Url' is the category, and the key should be an integer specifying " +
                                          $"the order the requests should be made. You may also specify the request to in the key by specifying the " +
                                          $"order integer, a colon, then the request type. Samples keys are '8', '1:POST', and '3:GET'. The value is the url.",
                    Required = true,
                    Type = "System.String",
                };

                var fileExtensionParameter = new ParameterModel()
                {
                    Key = "File Extension",
                    Description = "The file extension for the downloaded files.",
                    Required = true,
                    Type = "System.String",
                };

                JobModel sampleExtractor = new JobModel()
                {
                    Name = "Sample HttpDownloader",
                    Description = "Sample Extractor using HttpDownloader.",
                    ClassName = "Flourish.Services.HttpDownloader",
                    JobType = extractorJobType,
                    Parameters = new List<ParameterModel>()
                    {
                        urlParameter,
                        fileExtensionParameter,
                    }
                };

                JobModel sampleTransformer = new JobModel()
                {
                    Name = "Sample UselessTransformer",
                    Description = "Sample transformer job that does nothing.",
                    ClassName = "Flourish.Services.UselessTransformer",
                    JobType = transformerJobType,
                };

                JobModel sampleLoader = new JobModel()
                {
                    Name = "Sample UselessLoader",
                    Description = "Sample loader job that does nothing.",
                    ClassName = "Flourish.Services.UselessLoader",
                    JobType = loaderJobType,
                };

                context.Jobs.AddRange(sampleExtractor, sampleTransformer, sampleLoader);

                // Add a service
                ServiceModel sampleService = new ServiceModel()
                {
                    Name = "Sample Service",
                    Desciption = "Sample ETL service",
                    Active = true,
                    Priority = 5,
                    Schedule = cronSchedule,
                    Parameters = new List<ServiceParameterModel>() { },
                };

                context.Services.Add(sampleService);

                // Add the jobs to the service
                var extractorServiceJob = new ServiceJobModel()
                {
                    Service = sampleService,
                    Active = true,
                    ArchiveOutput = true,
                    Job = sampleExtractor,
                };

                var transformerServiceJob = new ServiceJobModel()
                {
                    Service = sampleService,
                    Active = true,
                    ArchiveOutput = true,
                    Job = sampleTransformer,
                };

                var loaderServiceJob = new ServiceJobModel()
                {
                    Service = sampleService,
                    Active = true,
                    ArchiveOutput = true,
                    Job = sampleLoader,
                };

                context.ServiceJobs.AddRange(extractorServiceJob, transformerServiceJob, loaderServiceJob);

                // Add the job mappings
                context.ServiceJobMappings.AddRange(ServiceJobMappingModel.CreateMappings(extractorServiceJob, transformerServiceJob, loaderServiceJob));

                // Create the required parameters for th service.
                sampleService.Parameters.Add(new ServiceParameterModel()
                {
                    Service = sampleService,
                    Parameter = urlParameter,
                    Category = urlParameter.Category,
                    Type = urlParameter.Type,
                    Key = "1",
                    Value = "https://www.ibm.com/support/knowledgecenter/en/SVU13_7.2.1/com.ibm.ismsaas.doc/reference/UsersImportSimpleSample.csv",
                });

                sampleService.Parameters.Add(new ServiceParameterModel()
                {
                    Service = sampleService,
                    Parameter = fileExtensionParameter,
                    Key = fileExtensionParameter.Key,
                    Type = fileExtensionParameter.Type,
                    Value = "csv",
                });

                sampleService.Parameters.Add(new ServiceParameterModel()
                {
                    Service = sampleService,
                    Parameter = cronExpressionParameter,
                    Key = cronExpressionParameter.Key,
                    Type = cronExpressionParameter.Type,
                    Value = "0 0/5 0 ? * * *",
                });

                context.SaveChanges();
                transaction.Commit();    
            }
        }
    }
}
