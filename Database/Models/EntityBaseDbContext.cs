﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Base class for DbContexts whose entities derive from EntityBase. This DbContext will automatically populate the metadata columns described in EntityBase.
    /// </summary>
    public class EntityBaseDbContext : DbContext
    {
        public EntityBaseDbContext()
            : base()
        {

        }

        public EntityBaseDbContext(DbContextOptions<ServicingContext> options)
            : base(options)
        {

        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            AddTimestamps();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            AddTimestamps();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// Updates the timestamp values for inserted and updated records.
        /// Code for updating the username is commented out because that data will mostly likely not be available to this project, and will has to be passed by the web app.
        /// 
        /// TODO: Make sure this doesn't impact performance in a noticable way.
        /// </summary>
        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is EntityBase && (x.State == EntityState.Added || x.State == EntityState.Modified));

            /*var currentUsername = !string.IsNullOrEmpty(System.Web.HttpContext.Current?.User?.Identity?.Name)
                ? HttpContext.Current.User.Identity.Name
                : "Anonymous";
                */
            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((EntityBase)entity.Entity).DateCreated = DateTime.UtcNow;
                    //((EntityBase)entity.Entity).UserCreated = currentUsername;
                }

                ((EntityBase)entity.Entity).DateModified = DateTime.UtcNow;
                //((EntityBase)entity.Entity).UserModified = currentUsername;
            }
        }
    }
}
