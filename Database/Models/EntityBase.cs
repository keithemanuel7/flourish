﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Base entity containing metadata fields.
    /// </summary>
    public class EntityBase
    {
        /// <summary>
        /// The date and time this record was created.
        /// </summary>
        public DateTimeOffset DateCreated { get; set; }

        /// <summary>
        /// The username of the person who created this record.
        /// </summary>
        //[Required]
        public string UserCreated { get; set; }

        /// <summary>
        /// The date and time this record was last modified.
        /// </summary>
        public DateTimeOffset DateModified { get; set; }

        /// <summary>
        /// The username of the person who last modified this record.
        /// </summary>
        //[Required]
        public string UserModified { get; set; }
    }
}
