﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// A log for monitoring ServiceJob execution.
    /// </summary>
    [Table("ServiceLog", Schema = "Servicing")]
    public class ServiceLogModel : EntityBase
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        [Key]
        public int ServiceLogId { get; set; }
        
        /// <summary>
        /// A foreign key for the Service or Service that owns the Job that created this record.
        /// </summary>
        public int ServiceId { get; set; }

        /// <summary>
        /// A foreign key for the ServiceJob that created this record. Not required, because a Service could make a log entry without a job.
        /// </summary>
        public int? ServiceJobId { get; set; }
        public ServiceJobModel ServiceJob { get; set; }

        /// <summary>
        /// The DateTimeOffset for this record.
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// The LogLevel for this record.
        /// </summary>
        [Required]
        public string LogLevel { get; set; }

        /// <summary>
        /// Optional EventType for common events.
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// Optional Message for this record.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Optional ExceptionLog foreign key for fatal and non-fatal exceptions.
        /// </summary>
        public int? ExceptionLogId { get; set; }
        public ExceptionLogModel ExceptionLog { get; set; }
    }
}
