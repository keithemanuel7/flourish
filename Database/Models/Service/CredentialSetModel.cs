﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a set of credentials.
    /// </summary>
    [Table("CredentialSet", Schema = "Servicing")]
    public class CredentialSetModel : EntityBase
    {
        /// <summary>
        /// Primary key
        /// </summary>
        [Key]
        public int CredentialSetId { get; set; }

        /// <summary>
        /// Brief label for the credentials.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Foreign key for the Certificate table.
        /// </summary>
        public int CertificateId { get; set; }
        public CertificateModel Certificate { get; set; }
    }
}
