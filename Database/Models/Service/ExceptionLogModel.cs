﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    [Table("ExceptionLog", Schema = "Servicing")]
    public class ExceptionLogModel : EntityBase
    {
        /// <summary>
        /// Primary key
        /// </summary>
        [Key]
        public int ExceptionLogId { get; set; }

        /// <summary>
        /// Exception type
        /// </summary>
        [Required]
        public string Type { get; set; }

        /// <summary>
        /// Exception message
        /// </summary>
        [Required]
        public string Message { get; set; }

        /// <summary>
        /// Exception stack trace
        /// </summary>
        [Required]
        public string StackTrace { get; set; }

        /// <summary>
        /// Reference to this exception's inner exception.
        /// </summary>
        public int? InnerExceptionLogId { get; set; }
        public ExceptionLogModel InnerException { get; set; }
    }

    /// <summary>
    /// Extension methods for ExceptionLogModel
    /// </summary>
    public static class ExceptionLogModelExtensions
    {
        /// <summary>
        /// Inserts an Exception and any inner exceptions into the DbSet. Recursive.
        /// </summary>
        /// <param name="dbSet">The DbSet to add to.</param>
        /// <param name="exception">The exception, containing any inner exceptions, to add.</param>
        /// <returns></returns>
        public static EntityEntry<ExceptionLogModel> InsertException(this Microsoft.EntityFrameworkCore.DbSet<ExceptionLogModel> dbSet, Exception exception)
        {
            ExceptionLogModel newExceptionLog = CreateExceptionLogModel(exception);
            return dbSet.Add(newExceptionLog);
        }

        /// <summary>
        /// Creates an ExceptionLogModel as well as its inner exceptions.
        /// </summary>
        /// <param name="exception">The outermost exception.</param>
        /// <returns>An ExceptionLogModel with its InnerExceptions populated.</returns>
        private static ExceptionLogModel CreateExceptionLogModel(Exception exception)
        {
            ExceptionLogModel newExceptionLog = new ExceptionLogModel()
            {
                Type = exception.GetType().Name,
                Message = exception.Message,
                StackTrace = exception.StackTrace,
                InnerException = CreateExceptionLogModel(exception.InnerException),
            };

            return newExceptionLog;
        }
    }
}
