﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes an X509 certificate.
    /// </summary>
    [Table("Certificate", Schema = "Servicing")]
    public class CertificateModel : EntityBase
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        public int CertificateId { get; set; }

        /// <summary>
        /// Binary representation of the certificate file.
        /// </summary>
        public byte[] File { get; set; }

        /// <summary>
        /// The file extension.
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// The certificate thumbprint.
        /// </summary>
        public string Thumbprint { get; set; }

    }
}
