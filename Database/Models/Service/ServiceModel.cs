﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a Service, which is a set of related Jobs.
    /// </summary>
    [Table("Service", Schema = "Servicing")]
    public class ServiceModel : EntityBase
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        [Key]
        public int ServiceId { get; set; }

        /// <summary>
        /// The name of the service.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// A description of the Service.
        /// </summary>
        [Required]
        public string Desciption { get; set; }

        /// <summary>
        /// Indicates whether this Service is active.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Foreign key for Schedule.
        /// </summary>
        public int ScheduleId { get; set; }
        public ScheduleModel Schedule { get; set; }

        /// <summary>
        /// Indicates the priority of this Service.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// A list of all Parameters for this Service, which includes Service, Job, and Schdule parameters. 
        /// </summary>
        public ICollection<ServiceParameterModel> Parameters { get; set; }
        
        /// <summary>
        /// A list of the Jobs that belong to this Service.
        /// </summary>
        public ICollection<ServiceJobModel> ServiceJobs { get; set; }
    }
}
