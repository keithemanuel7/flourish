﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a Job or Schedule parameter, which may be required or optional.
    /// </summary>
    /// <remarks>
    /// This describes a key-value pair, but the value is not provided here. The values are in the table ServiceParameter.
    /// A Parameter may belong to either a Job or a Schedule, but the values for these parameters are given at the service level.
    /// For example, a Service that has a Job that downloads from a URL would have a required URL parameters. That Job could have
    /// a Schedule, which might have a required CronExpression parameter.
    /// </remarks>
    [Table("Parameter", Schema = "Servicing")]
    public class ParameterModel : EntityBase
    {
        /// <summary>
        /// Primary key
        /// </summary>
        [Key]
        public int ParameterId { get; set; }

        /// <summary>
        /// Job foreign key. Should not be null if ScheduleId is null.
        /// </summary>
        public int? JobId { get; set; }
        public JobModel Job { get; set; }

        /// <summary>
        /// Schedule foreign key. Should not be null if JobId is null.
        /// </summary>
        public int? ScheduleId { get; set; }
        public ScheduleModel Schedule { get; set; }

        /// <summary>
        /// Optional parameter category. Must be provided if a Key is not provided.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Optional Parameter key for this key-value pair. Must be provided if a Category is not provided.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// The data type of this parameter. For example, System.String.
        /// </summary>
        [Required]
        public string Type { get; set; }

        /// <summary>
        /// A boolean value indicating whether this parameter is required for execution.
        /// </summary>
        [Required]
        public bool Required { get; set; }

        /// <summary>
        /// Parameter description.
        /// </summary>
        public string Description { get; set; }
    }
}
