﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a file extracted and where it currently is in processing.
    /// </summary>
    public class FileLog : EntityBase
    {
        /// <summary>
        /// The job that extracted or created this file.
        /// </summary>
        public int ExtractorServiceJobId { get; set; }
        public ServiceJobModel ExtractorServiceJob { get; set; }
        
        /// <summary>
        /// The ServiceJob that is the next step in processing. Will be null when processing this file is complete.
        /// </summary>
        public int? NextServiceJobId { get; set; }
        public ServiceJobModel NextServiceJob { get; set; }

        /// <summary>
        /// The file name.
        /// </summary>
        [Required]
        public string FileName { get; set; }

        /// <summary>
        /// The full path to the file. Will be null if the file was not archived.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// The date of when the file was extracted.
        /// </summary>
        [Required]
        public DateTime FileDate { get; set; }

        /// <summary>
        /// Whether the file has been completely processed.
        /// </summary>
        public bool ProcessingComplete { get; set; }
    }
}
