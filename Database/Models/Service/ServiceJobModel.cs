﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes the relationship between a Service and its Jobs.
    /// </summary>
    [Table("ServiceJob", Schema = "Servicing")]
    public class ServiceJobModel : EntityBase
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        [Key]
        public int ServiceJobId { get; set; }

        /// <summary>
        /// Foreign key for the Service that owns the Job.
        /// </summary>
        public int ServiceId { get; set; }
        public ServiceModel Service { get; set; }

        /// <summary>
        /// Foreign key for the Job that is owned by the Service.
        /// </summary>
        public int JobId { get; set; }
        public JobModel Job { get; set; }

        /// <summary>
        /// Indicates whether the Job is active.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Indicates whether the Job's output should be archived, usually by saving permanently to the file storage.
        /// </summary>
        public bool ArchiveOutput { get; set; }

        /// <summary>
        /// Indicates the number of times to reattempt a failed job.
        /// </summary>
        public int RetryAttempts { get; set; }

        /// <summary>
        /// Indicates the amount of time to wait between retries.
        /// </summary>
        [Required]
        public TimeSpan RetryTimespan { get; set; }

        /// <summary>
        /// A collection of all of the ServiceJobMappings where this service is a child to a parent.
        /// </summary>
        public ICollection<ServiceJobMappingModel> ParentServiceJobMappings { get; set; }

        /// <summary>
        /// A collection of all of the ServiceJobMappings where this service is a parent to a child.
        /// </summary>
        public ICollection<ServiceJobMappingModel> ChildServiceJobMappings { get; set; }

        [NotMapped]
        public IJob JobInstance { get; set; }

        /// <summary>
        /// Returns a list of this ServiceJob's Parent ServiceJobs.
        /// </summary>
        [NotMapped]
        public IList<ServiceJobModel> ParentServiceJobs
        {
            get
            {
                return ParentServiceJobMappings
                    .Select(p => p.ParentServiceJob)
                    .ToList();
            }
        }

        /// <summary>
        /// Returns a list of this ServiceJob's Child ServiceJobs.
        /// </summary>
        [NotMapped]
        public IList<ServiceJobModel> ChildServiceJobs
        {
            get
            {
                return ChildServiceJobMappings
                    .Select(c => c.ChildServiceJob)
                    .ToList();
            }
        }
    }

    public interface IJob
    {
        /// <summary>
        /// Triggers an execution of the Job.
        /// </summary>
        void Trigger();
    }
}
