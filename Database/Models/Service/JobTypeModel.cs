﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a JobType. For example, Extractor, Transformer, Loader, or Task.
    /// Right now, this isn't much more than a label.
    /// </summary>
    public class JobTypeModel : EntityBase
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        [Key]
        public int JobTypeId { get; set; }

        /// <summary>
        /// A brief label describing the JobType.
        /// </summary>
        public string Name { get; set; }
    }
}
