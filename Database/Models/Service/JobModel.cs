﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a Job, which is part of a Service. Whenever possible, Jobs should be written to be resuable.
    /// Services are mapped to jobs by ServiceJobModel, and to other ServiceJobs by ServiceJobMappingModel.
    /// </summary>
    [Table("Job", Schema = "Servicing")]
    public class JobModel : EntityBase
    {
        /// <summary>
        /// Primary key.
        /// </summary>
        [Key]
        public int JobId { get; set; }

        /// <summary>
        /// Foreign key for JobType. 
        /// </summary>
        public int JobTypeId { get; set; }
        public JobTypeModel JobType { get; set; }

        /// <summary>
        /// The list of Parameters for this Job.
        /// </summary>
        public ICollection<ParameterModel> Parameters { get; set; }
        
        /// <summary>
        /// The Jobs name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The Jobs description.
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// The name of the C# class that executes this Job.
        /// </summary>
        [Required]
        public string ClassName { get; set; }
    }
}
