﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a Parameter that belongs to a Service, the Service's Jobs, or those Job's Schedules.
    /// </summary>
    [Table("ServiceParameter", Schema = "Servicing")]
    public class ServiceParameterModel : EntityBase
    {
        /// <summary>
        /// Primary key
        /// </summary>
        [Key]
        public int ServiceParameterId { get; set; }
        
        /// <summary>
        /// Foreign key indicating the Service that owns this ServiceParameter.
        /// </summary>
        public int ServiceId { get; set; }
        public ServiceModel Service { get; set; }
        
        /// <summary>
        /// Foreign key indicating what, if any, Job or Schedule this ServiceParameter belongs to.
        /// </summary>
        public int? ParameterId { get; set; }
        public ParameterModel Parameter { get; set; }

        /// <summary>
        /// This ServiceParameter's category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// This ServiceParameter's key.
        /// </summary>
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// This ServiceParameter's value
        /// </summary>
        [Required]
        public string Value { get; set; }

        /// <summary>
        /// The Type (data type) that this ServiceParameter's value will be converted into at runtime.
        /// </summary>
        [Required]
        public string Type { get; set; }
    }
}
