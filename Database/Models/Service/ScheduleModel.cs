﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Describes a Schedule, and specifies which C# class implements this Schedule.
    /// </summary>
    [Table("Schedule", Schema = "Servicing")]
    public class ScheduleModel : EntityBase
    {
        /// <summary>
        /// Primary Key.
        /// </summary>
        [Key]
        public int ScheduleId { get; set; }

        /// <summary>
        /// The Schedule's name.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// A description of the Schedule.
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// The name of the C# class that implements this Schedule's logic.
        /// </summary>
        [Required]
        public string ClassName { get; set; }
    }
}
