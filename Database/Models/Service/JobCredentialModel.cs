﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Maps a ServiceJob to a CredentailSet.
    /// </summary>
    [Table("JobCredential", Schema = "Servicing")]
    public class JobCredentialModel : EntityBase
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        [Key]
        public int JobCredentialId { get; set; }

        /// <summary>
        /// Foreign key for ServiceJob
        /// </summary>
        public int ServiceJobId { get; set; }
        public ServiceJobModel ServiceJob { get; set; }

        /// <summary>
        /// Foreign key for the CredentialSet
        /// </summary>
        public int CredentialSetId { get; set; }
        public CredentialSetModel CredentialSet { get; set; }
    }
}
