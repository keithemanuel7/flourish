﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Flourish.Database.Models
{
    public class ServicingContext : EntityBaseDbContext
    {
        public ServicingContext()
        { }

        public ServicingContext(DbContextOptions<ServicingContext> options)
            : base(options)
        {

        }

        public DbSet<ServiceModel> Services { get; set; }
        public DbSet<JobModel> Jobs { get; set; }
        public DbSet<ServiceJobModel> ServiceJobs { get; set; }
        public DbSet<ServiceJobMappingModel> ServiceJobMappings { get; set; }
        public DbSet<JobTypeModel> JobTypes { get; set; }
        public DbSet<JobCredentialModel> JobCredentials { get; set; }
        public DbSet<ParameterModel> JobParameters { get; set; }
        public DbSet<ServiceParameterModel> ServiceParameters { get; set; }
        public DbSet<CredentialSetModel> CredentialSets { get; set; }
        public DbSet<ScheduleModel> Schedules { get; set; }
        public DbSet<CertificateModel> Certificates { get; set; }
        public DbSet<ServiceLogModel> ServiceLogs { get; set; }
        public DbSet<FileLog> FileLogs { get; set; }
        public DbSet<ExceptionLogModel> ExceptionLogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Servicing;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServiceJobMappingModel>()
                .HasOne(sjm => sjm.ParentServiceJob)
                .WithMany(sj => sj.ParentServiceJobMappings)
                .HasForeignKey(sjm => sjm.ParentServiceJobId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ServiceJobMappingModel>()
                .HasOne(sjm => sjm.ChildServiceJob)
                .WithMany(sj => sj.ChildServiceJobMappings)
                .HasForeignKey(sjm => sjm.ChildServiceJobId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
