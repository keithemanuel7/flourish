﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Flourish.Database.Models
{
    /// <summary>
    /// Maps the many to many relationships between a Service's Jobs.
    /// </summary>
    [Table("ServiceJobMapping", Schema = "Servicing")]
    public class ServiceJobMappingModel : EntityBase
    {
        /// <summary>
        /// Primary Key.
        /// </summary>
        [Key]
        public int ServiceJobMappingId { get; set; }

        /// <summary>
        /// The parent ServiceJob, which is executed prior to the child.
        /// </summary>
        public int ParentServiceJobId { get; set; }
        public ServiceJobModel ParentServiceJob { get; set; }

        /// <summary>
        /// The child ServiceJob, whose execution follows the parent.
        /// </summary>
        public int ChildServiceJobId { get; set; }
        public ServiceJobModel ChildServiceJob { get; set; }

        public static IList<ServiceJobMappingModel> CreateMappings(params ServiceJobModel[] serviceJobs)
        {
            var mappings = new List<ServiceJobMappingModel>();

            for(int i = 0; i < serviceJobs.Length - 1; ++i)
            {
                mappings.Add(new ServiceJobMappingModel()
                {
                    ParentServiceJob = serviceJobs[i],
                    ChildServiceJob = serviceJobs[i + 1],
                });
            }

            return mappings;
        }
    }
}